﻿using System;

namespace LargeAddition
{
    class Program
    {
        static void Main(string[] args)
        {
            string number1;
            string number2;
            int carry = 0;
            string reverseOutput = String.Empty;

            // Read two numbers from user
            number1 = Console.ReadLine();
            number2 = Console.ReadLine();

            // Before starting make sure length of str2 is larger than str1 (easier to add).
            if (number1.Length > number2.Length)
            {
                string temp = number2;
                number2 = number1;
                number1 = temp;
            }

            // Reverse the number so as to perform addition from back
            number1 = Reverse(number1);
            number2 = Reverse(number2);

            
            

            // Loop until the smaller number is fully added.
            for (int i = 0; i < number1.Length; i++)
            {
                // Add corresponding index digits of number1 and number2.
                int sumOfDigits = (number1[i] - '0') + (number2[i] - '0') + carry;

                // Concatenate the output with the addition result of above digits.
                reverseOutput = reverseOutput + (char) (sumOfDigits % 10 + '0');

                // Carry for next index addition.
                carry = sumOfDigits / 10;
                
            }

            // Add the remaining digits in the larger number.
            for (int i = number1.Length; i < number2.Length; i++)
            {
                int sumOfDigits = (number2[i] - '0' + carry);
                reverseOutput = reverseOutput + (char) (sumOfDigits % 10 + '0');                
                carry = (sumOfDigits / 10);
            }

            // At the end if still any carry remains, add it to result.
            if (carry != 0)
            {
                reverseOutput = reverseOutput + carry;
            }
            

            string output = Reverse(reverseOutput);

            Console.WriteLine("\n" + output);            
        }

        // Function to reverse a string.
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

    }
}