﻿using System;

namespace ASCIIConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            // Take input as string.
            string input;

            Console.WriteLine("Enter the String: ");
            input = Console.ReadLine();

            // Final output will be stored in output.
            string output = String.Empty;

            // Iterates through input string character by character.
            for (int i = 0; i < input.Length-1; i++)
            {
                int average = (input[i] + input[i + 1]) / 2;

                // If number is prime then replace with next consecutive number.
                if (IsPrime(average))
                {
                    average++;
                }

                // Stores output with each iteration
                output = output + Convert.ToChar(average);

            }

            // Prints output
            Console.WriteLine(output);
        }

        // Function for checking a number is prime or not.
        public static bool IsPrime(int number)
        {
            int isDivisible = 0;
            
            // Loop iterates from 2 until the input number.
            for (int i = 2; i < number; i++)
            {
                // Checks if input number is divisble by any number in the loop.
                if (number % i == 0)
                {
                    isDivisible++;
                    break;

                }
            }

            bool primeCheckerValue = false;

            // If number was not mutiple of any number in previous loop,
            // then its a prime number.
            if (isDivisible == 0)
            {
                primeCheckerValue = true;
            }

            return primeCheckerValue;
        }
    }
}

