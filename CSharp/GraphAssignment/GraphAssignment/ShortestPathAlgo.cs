﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphAssignment
{
    public class ShortestPathAlgo
    {
        public int MAX = 9999;  // Maximum value to compare distance
        public int[] Distance;  // Distance from a particular index

        public StringBuilder finalPath = new StringBuilder(); // Stores the final path

        // Recursive function to add shortest path into the final anwser
        public void MakePath(int[] parent, int destination)
        {
            if (parent[destination] == -1)
            {
                return;
            }

            MakePath(parent, parent[destination]);
            
            finalPath.Append(destination);

        }

        // Function to find minimum distance from a vertex
        public int MinDistance(int[] Distance, bool[] pickIndex)
        {
            int min = MAX;
            int minIndex = 0;
            int v = Distance.Length;
            for (int i = 0; i < v; i++)
            {
                if (pickIndex[i] == false && (Distance[i] <= min))
                {
                    min = Distance[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }

        public void Dijkstra(int[,] Weight, int source, int destination)
        {
            int vertices = Weight.GetLength(0);
            Distance = new int[vertices]; // shortest distance from vertex to another
            bool[] visitedVertex = new bool[vertices]; // if true: corresponding vertex is included in path;
            int[] Parent = new int[vertices];

            Parent[source] = -1;

            for (int i = 0; i < vertices; i++)
            {
                Distance[i] = MAX;
                visitedVertex[i] = false;
            }
            
            Distance[source] = 0;

            for (int i = 0; i < vertices - 1; i++)
            {
                int minDist = MinDistance(Distance, visitedVertex);
                visitedVertex[minDist] = true;

                // Loop runs for all vertices
                for (int j = 0; j < vertices; j++)
                {
                    if (!(visitedVertex[j]) && (Weight[minDist, j] != -1) && (Distance[minDist] + Weight[minDist, j] < Distance[j]))
                    {
                        Parent[j] = minDist;
                        Distance[j] = Distance[minDist] + Weight[minDist, j];
                    }
                }
            }
            // Add new vertex to the ongoing path
            finalPath.Append(source);

            MakePath(Parent, destination);
        }
    }
}