﻿using System;

namespace GraphAssignment
{
    public class DataStructure
    {
        // Structure to store label name for indexes
        public struct VertexName
        {
           public int label;
        }

        public int totalVertices; // total vertices in the graph
        public int[ , ] Weights; // stores weight of edges
        public VertexName[] LabelList; // array to store Label names

        // Constructor to intialize and declare following variables
        public DataStructure(int vertices)
        {
            totalVertices = vertices;
            Weights = new int[vertices, vertices];
            LabelList = new VertexName[vertices];

            // Intialize all edges distances to -1
            for (int i = 0; i < vertices; i++)
            {
                for (int j = 0; j < vertices; j++)
                {
                    Weights[i, j] = -1; 
                }
            }                

        }

        // Function to get weight in between two vertices
        public void GetWeight(int labelX , int labelY)
        {
            int Xindex = GetIndex(labelX);
            int Yindex = GetIndex(labelY);

            Console.WriteLine(Weights[Xindex, Yindex]);
        }

        // Function to name the vertices to a label to match like question
        public void SetLabel(int index, int name)
        {
            LabelList[index] = new VertexName
            {
                label = name
            };
        }

        // Function to get index from corresponding label
        public int GetIndex(int name)
        {
            for (int i = 0; i < totalVertices; i++)
            {
                if(LabelList[i].label == name)
                {
                    return i;
                }
            }
            return -1;
        }

        // Function to get Label from corresponding index
        public int GetLabel(int index)
        {
            return LabelList[index].label ;
        }

        // Function to add Edge weight between two vertices
        public void AddEdge(int u, int v, int weight)
        {
            int x = GetIndex(u);
            int y = GetIndex(v);
            Weights[x, y] = weight;
            Weights[y, x] = weight;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            // Create Data Structure for storing graph
            DataStructure myGraph = new DataStructure(10);

            // Set Label corresponding to Assignment Question
            myGraph.SetLabel(0, 1);
            myGraph.SetLabel(1, 2);
            myGraph.SetLabel(2, 3);
            myGraph.SetLabel(3, 4);
            myGraph.SetLabel(4, 5);
            myGraph.SetLabel(5, 6);
            myGraph.SetLabel(6, 9);
            myGraph.SetLabel(7, 10);
            myGraph.SetLabel(8, 12);
            myGraph.SetLabel(9, 13);

            // Add edges and their corresponding weights between two vertices
            myGraph.AddEdge(1, 2, 1);
            myGraph.AddEdge(3, 1, 2);
            myGraph.AddEdge(2, 5, 3);
            myGraph.AddEdge(3, 4, 1);
            myGraph.AddEdge(5, 6, 1);
            myGraph.AddEdge(6, 4, 2);
            myGraph.AddEdge(5, 9, 4);
            myGraph.AddEdge(4, 12, 8);
            myGraph.AddEdge(9, 10, 1);            
            myGraph.AddEdge(12, 13, 2); 
            myGraph.AddEdge(9, 12, 3);
            myGraph.AddEdge(10, 13, 3);

            // Create class for finding shortest path
            ShortestPathAlgo findPath = new ShortestPathAlgo();

            // Input from user for the source and destination
            Console.Write("Source: ");
            int sourceByUser = Convert.ToInt32(Console.ReadLine());

            Console.Write("Destination: ");
            int destinationByUser = Convert.ToInt32(Console.ReadLine());

            // Get index of the label from the data structure stored
            int source = myGraph.GetIndex(sourceByUser);
            int destination = myGraph.GetIndex(destinationByUser);

            // Run algorithm for finding shortest path
            findPath.Dijkstra(myGraph.Weights, source, destination);

            // Path in terms of indexes. Note: not label (need to convert into label)
            string path = Convert.ToString(findPath.finalPath);

            Console.Write("Shortest path is : ");
            for (int i = 0; i < path.Length; i++)
            {
                if (i != 0)
                {
                    Console.Write("->");
                }

                int index = Int32.Parse(path[i].ToString());                
                Console.Write(myGraph.GetLabel(index));

               

            }

            Console.WriteLine("\nMinimum Distance = " + findPath.Distance[destination]);
            
        }
    }
}
