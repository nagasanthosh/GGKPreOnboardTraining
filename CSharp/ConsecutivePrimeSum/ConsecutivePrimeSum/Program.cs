﻿using System;

namespace ConsecutivePrimeSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int input;  // input number range
            int summation;
            int consecutivePrimeSumCount = 0; // Consecutive prime sum count

            Console.WriteLine("Enter Number: ");
            input = Convert.ToInt32(Console.ReadLine());
            
            // Loop itreates through all numbers from 3 to N
            for (int i = 3; i <= input; i++)
            {
                if (IsPrime(i))
                {
                    // Set summation = 2 before analysing any consecutive prime sum.
                    summation = 2;
                    for (int j = 3; j <= i; j++)
                    {
                        if ((summation <= i) && (IsPrime(j)))
                        {
                            summation += j;

                            if (summation == i)
                            {
                                consecutivePrimeSumCount++;
                            }
                        }
                    }
                }
            }

            // Prints the solution.
            Console.WriteLine(consecutivePrimeSumCount);
        }


        // Function for checking a number is prime or not.
        public static bool IsPrime(int number)
        {
            int isDivisible = 0;

            // Loop iterates from 2 until the input number.
            for (int i = 2; i < number; i++)
            {
                // Checks if input number is divisble by any number in the loop.
                if (number % i == 0)
                {
                    isDivisible++;
                    break;
                }
            }

            bool primeCheckerValue = false;

            // If number was not mutiple of any number in previous loop,
            // then its a prime number.
            if (isDivisible == 0)
            {
                primeCheckerValue = true;
            }

            return primeCheckerValue;
        }
    }
}
