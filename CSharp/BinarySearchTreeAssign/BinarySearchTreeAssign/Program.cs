﻿using System;

namespace BinarySearchTreeAssign
{
    // A Node class to store elements's data and its children's address.
    public class Node
    {
        public int data;
        public Node right;
        public Node left;
    }
     
    public class BinarySearchTree
    {
        // Function for inserting element into the Binary search tree.
        public Node InsertNode(Node root, int number)
        {
            if(root == null)
            {
                root = new Node
                {
                    data = number
                };
            }
            if(number < root.data)
            {
                root.left = InsertNode(root.left, number);
            }
            if(number> root.data)
            {
                root.right = InsertNode(root.right, number);
            }

            return root;
           
        }

        // Function for printing elements in ascending order.
        public void AscendingOrder(Node root)
        {
            if( root != null){
                AscendingOrder(root.left);
                Console.Write(root.data + " ");
                AscendingOrder(root.right);
            }            
        }

        // Function for printing elements in descending order.
        public void DescendingOrder(Node root)
        {
            if (root != null)
            {
                DescendingOrder(root.right);
                Console.Write(root.data + " ");
                DescendingOrder(root.left);
            }
        }

        // Function for searching element in the tree.
        public void SearchKey(Node root, int key)
        {
            if(root == null)
            {
                Console.WriteLine("Element not found");
            }
            else
            {
                if (root.data == key)
                {
                    Console.WriteLine("Element found");
                }
                else if(key < root.data)
                {
                    SearchKey(root.left, key);
                }
                else if (key > root.data ){
                    SearchKey(root.right, key);
                }
            }
        }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            // New instance of Binary tree created.
            BinarySearchTree myTree = new BinarySearchTree();
                        
            Node root = null;

            // Inserting elements as given in Assignment question.
            root = myTree.InsertNode(root, 7);
            root = myTree.InsertNode(root, 3);
            root = myTree.InsertNode(root, 11);
            root = myTree.InsertNode(root, 1);
            root = myTree.InsertNode(root, 5);
            root = myTree.InsertNode(root, 9);
            root = myTree.InsertNode(root, 13);
            root = myTree.InsertNode(root, 4);
            root = myTree.InsertNode(root, 6);
            root = myTree.InsertNode(root, 8);
            root = myTree.InsertNode(root, 12);
            root = myTree.InsertNode(root, 14);


            Console.WriteLine("choose:");
            Console.WriteLine("1. Print ascending 2. Print descending 3. Search an element");

            // Option for choosing any of the above function.
            int option = Convert.ToInt32(Console.ReadLine());


            switch (option)
            {
                case 1:
                    {
                        // Prints tree elements in ascending order.
                        if (root == null)
                        {
                            Console.WriteLine("No elements in the tree");
                        }
                        else
                        {
                            myTree.AscendingOrder(root);
                            Console.WriteLine();
                        }
                    }
                    break;
                case 2:
                    {
                        // Prints tree elements in descending order.
                        if (root == null)
                        {
                            Console.WriteLine("No elements in the tree");

                        }
                        else
                        {
                            myTree.DescendingOrder(root);
                            Console.WriteLine();
                        }
                    }
                    break;
                case 3:
                    {
                        // Searchs any element on existing tree.
                        Console.Write("Enter the element to be searched: ");
                        int key = Convert.ToInt32(Console.ReadLine());
                        myTree.SearchKey(root, key);
                    }
                    break;
                default:
                    {
                        //If no other option is choosen.
                        Console.WriteLine("Wrong Option. Try Again!");
                    }
                    break;

            }

        }
    }
}
